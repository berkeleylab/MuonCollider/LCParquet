#include "LCParquet/TrackerHitWriter.hxx"

#include <lcio.h>

#include <EVENT/LCCollection.h>
#include <EVENT/TrackerHit.h>

TrackerHitWriter::TrackerHitWriter(std::shared_ptr<arrow::fs::FileSystem> fs)
  : CollectionWriter()
{
  _file=std::make_shared<TrackerHitFile>();
  _file->init(fs, "trackerhit.parquet");
}

TrackerHitWriter::~TrackerHitWriter()
{
  _file->close();
}

void TrackerHitWriter::fill(const EVENT::LCCollection* col, const EVENT::LCEvent* evt )
{
  if( !col ) return;

  if( col->getTypeName() != lcio::LCIO::TRACKERHIT &&
      col->getTypeName() != lcio::LCIO::TRACKERHITPLANE &&
      col->getTypeName() != lcio::LCIO::TRACKERHITZCYLINDER )
    {
      throw EVENT::Exception( "Invalid collection type: "+ col->getTypeName() ) ; 
    }

  for(int i=0, n=col->getNumberOfElements() ; i < n ; ++i)
    {
      EVENT::LCObject *obj=col->getElementAt(i) ;
      _file->fill(obj, evt);
    }
}
