#include "LCParquet/ObjectFile.hxx"

#include <parquet/exception.h>
#include <parquet/properties.h>

#include "LCParquet/Defines.hxx"

void ObjectFile::init(std::shared_ptr<arrow::fs::FileSystem> fs, const std::string& path)
{
  std::shared_ptr<arrow::io::OutputStream> outfile;

  PARQUET_ASSIGN_OR_THROW(
			  outfile,
			  fs->OpenOutputStream(path));

  //
  // File Schema
  std::vector<std::shared_ptr<arrow::Field>> fields = {
    arrow::field("evt"   , arrow::int32()),
    arrow::field("colidx", arrow::int64())
  };

  const std::vector<std::shared_ptr<arrow::Field>>& colfields=this->fields();
  fields.insert(fields.end(), colfields.begin(), colfields.end());

  _schema=arrow::schema(fields);

  if(_numrowsingroup<0)
    { _numrowsingroup=32*1024*1024/(4*fields.size()); }

  // Create writer
  parquet::WriterProperties::Builder writer_properties;
  writer_properties.compression(parquet::Compression::SNAPPY);

  parquet::arrow::FileWriter::Open(*_schema, ::arrow::default_memory_pool(),
                                   outfile,
				   writer_properties.build(),
                                   parquet::default_arrow_writer_properties(),
                                   &_writer);

}

void ObjectFile::close()
{
  write();
  _writer->Close();
}

void ObjectFile::fill(const EVENT::LCObject* obj, const EVENT::LCEvent* evt)
{
  PARQUET_THROW_NOT_OK(_evt_builder   .Append(evt->getEventNumber()));
  PARQUET_THROW_NOT_OK(_colidx_builder.Append(obj->ext<CollIndex>()));

  ObjectFile::endrow();
}

void ObjectFile::endrow()
{
  _rows++;

  if((_rows%_numrowsingroup)==0)
    {
      write();
    }
}

void ObjectFile::write()
{
  std::shared_ptr<arrow::Array> evt_array;
  PARQUET_THROW_NOT_OK(_evt_builder   .Finish(&evt_array   ));
  std::shared_ptr<arrow::Array> colidx_array;
  PARQUET_THROW_NOT_OK(_colidx_builder.Finish(&colidx_array));

  std::vector<std::shared_ptr<arrow::Array>> arrays = {evt_array, colidx_array};
  const std::vector<std::shared_ptr<arrow::Array>>& colarrays=this->arrays();
  arrays.insert(arrays.end(), colarrays.begin(), colarrays.end());

  // Make table and write
  std::shared_ptr<arrow::Table> table
    = arrow::Table::Make(_schema, arrays);

  _writer->WriteTable(*table, evt_array->length());
}

