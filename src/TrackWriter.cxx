#include "LCParquet/TrackWriter.hxx"

#include <lcio.h>

#include <EVENT/LCCollection.h>
#include <EVENT/Track.h>

#include "LCParquet/Defines.hxx"

TrackWriter::TrackWriter(std::shared_ptr<arrow::fs::FileSystem> fs)
  : CollectionWriter()
{
  _tr=std::make_shared<TrackFile>();
  _tr->init(fs, "track.parquet");

  _ts=std::make_shared<TrackStateFile>();
  _ts->init(fs, "trackstate.parquet");
}

TrackWriter::~TrackWriter()
{
  _tr->close();
  _ts->close();
}

void TrackWriter::fill(const EVENT::LCCollection* col, const EVENT::LCEvent* evt)
{
  if( !col ) return;

  if( col->getTypeName() != lcio::LCIO::TRACK )
    {
      throw EVENT::Exception( "Invalid collection type: "+ col->getTypeName() ) ; 
    }

  int tscnt=0;
  for(int i=0, n=col->getNumberOfElements() ; i < n ; ++i)
    {
      EVENT::LCObject *obj=col->getElementAt(i);

      // Index the track states
      const EVENT::Track* trk = dynamic_cast<const EVENT::Track*>( obj ) ;
      if(trk==0)
	{ throw EVENT::Exception( "Invalid object type." ) ; }

      const EVENT::TrackStateVec &tss = trk->getTrackStates();
      for(int j=0; j<tss.size(); ++j)
       	{ tss[j]->ext<CollIndex>() = tscnt++; }

      // Fill
      _tr->fill(obj, evt);
      for(const EVENT::TrackState* ts : tss)
       	{ _ts->fill(ts, evt); }
    }
}
