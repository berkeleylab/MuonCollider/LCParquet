#include "LCParquet/TrackStateFile.hxx"

#include <EVENT/Track.h>

#include "LCParquet/Defines.hxx"

std::vector<std::shared_ptr<arrow::Field>> TrackStateFile::fields()
{
  std::vector<std::shared_ptr<arrow::Field>> fields = {
    arrow::field("loc",arrow::int32  ()),
    arrow::field("dze",arrow::float32()),
    arrow::field("phi",arrow::float32()),
    arrow::field("ome",arrow::float32()),
    arrow::field("zze",arrow::float32()),
    arrow::field("tnl",arrow::float32()),
    arrow::field("rpx",arrow::float32()),
    arrow::field("rpy",arrow::float32()),
    arrow::field("rpz",arrow::float32()),
    arrow::field("covdzedze",arrow::float32()),
    arrow::field("covphidze",arrow::float32()),
    arrow::field("covphiphi",arrow::float32()),
    arrow::field("covomedze",arrow::float32()),
    arrow::field("covomephi",arrow::float32()),
    arrow::field("covomeome",arrow::float32()),
    arrow::field("covzzedze",arrow::float32()),
    arrow::field("covzzephi",arrow::float32()),
    arrow::field("covzzeome",arrow::float32()),
    arrow::field("covzzezze",arrow::float32()),
    arrow::field("covtnldze",arrow::float32()),
    arrow::field("covtnlphi",arrow::float32()),
    arrow::field("covtnlome",arrow::float32()),
    arrow::field("covtnlzze",arrow::float32()),
    arrow::field("covtnltnl",arrow::float32()),
  };

  return fields;
}


std::vector<std::shared_ptr<arrow::Array>> TrackStateFile::arrays()
{
  std::vector<std::shared_ptr<arrow::Array>> arrays = {};
  std::shared_ptr<arrow::Array> array;

  PARQUET_THROW_NOT_OK(_loc_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_dze_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_phi_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_ome_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_zze_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_tnl_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_rpx_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_rpy_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_rpz_builder  .Finish(&array   ));
  arrays.push_back(array);

  for(int j=0;j<15;++j) {
    PARQUET_THROW_NOT_OK(_cov_builder[j].Finish(&array   ));
    arrays.push_back(array);
  }

  return arrays;
}

void TrackStateFile::fill(const EVENT::LCObject* obj, const EVENT::LCEvent* evt)
{
  const EVENT::TrackState* ts = dynamic_cast<const EVENT::TrackState*>( obj ) ;
  if(ts==0)
    { throw EVENT::Exception( "Invalid object type." ) ; }
 
  // Track State specific
  PARQUET_THROW_NOT_OK(_loc_builder  .Append(ts->getLocation()));
  PARQUET_THROW_NOT_OK(_dze_builder  .Append(ts->getD0()));
  PARQUET_THROW_NOT_OK(_phi_builder  .Append(ts->getPhi()));
  PARQUET_THROW_NOT_OK(_ome_builder  .Append(ts->getOmega()));
  PARQUET_THROW_NOT_OK(_zze_builder  .Append(ts->getZ0()));
  PARQUET_THROW_NOT_OK(_tnl_builder  .Append(ts->getTanLambda()));
  PARQUET_THROW_NOT_OK(_rpx_builder  .Append(ts->getReferencePoint()[0]));
  PARQUET_THROW_NOT_OK(_rpy_builder  .Append(ts->getReferencePoint()[1]));
  PARQUET_THROW_NOT_OK(_rpz_builder  .Append(ts->getReferencePoint()[2]));
    
  for(int j=0;j<15;++j) {
    PARQUET_THROW_NOT_OK(_cov_builder[j].Append(ts->getCovMatrix()[j]));
  }

  // Common
  ObjectFile::fill(obj, evt);
}
