#include "LCParquet/SimTrackerHitFile.hxx"

#include <EVENT/LCCollection.h>
#include <EVENT/SimTrackerHit.h>
#include <EVENT/MCParticle.h>

#include <chrono>

#include "LCParquet/Defines.hxx"

std::vector<std::shared_ptr<arrow::Field>> SimTrackerHitFile::fields()
{
  std::vector<std::shared_ptr<arrow::Field>> fields = {
    arrow::field("ci0",   arrow::int32  ()),
    arrow::field("ci1",   arrow::int32  ()),
    arrow::field("pox",   arrow::float64()),
    arrow::field("poy",   arrow::float64()),
    arrow::field("poz",   arrow::float64()),
    arrow::field("edp",   arrow::float32()),
    arrow::field("tim",   arrow::float32()),
    arrow::field("ptl",   arrow::float32()),
    arrow::field("mcidx", arrow::int64  ())
  };

  return fields;
}

std::vector<std::shared_ptr<arrow::Array>> SimTrackerHitFile::arrays()
{
  std::vector<std::shared_ptr<arrow::Array>> arrays = {};
  std::shared_ptr<arrow::Array> array;

  PARQUET_THROW_NOT_OK(_ci0_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_ci1_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_pox_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_poy_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_poz_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_edp_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_tim_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_ptl_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_mcidx_builder.Finish(&array   ));
  arrays.push_back(array);

  return arrays;
}

void SimTrackerHitFile::fill(const EVENT::LCObject* obj, const EVENT::LCEvent* evt)
{
  const EVENT::SimTrackerHit* hit = dynamic_cast<const EVENT::SimTrackerHit*>( obj ) ;
  if(hit==0)
    { throw EVENT::Exception( "Invalid object type." ) ; }

  // hit specific
  PARQUET_THROW_NOT_OK(_ci0_builder  .Append(hit->getCellID0   ()   ));
  PARQUET_THROW_NOT_OK(_ci1_builder  .Append(hit->getCellID1   ()   ));
  PARQUET_THROW_NOT_OK(_pox_builder  .Append(hit->getPosition  ()[0]));
  PARQUET_THROW_NOT_OK(_poy_builder  .Append(hit->getPosition  ()[1]));
  PARQUET_THROW_NOT_OK(_poz_builder  .Append(hit->getPosition  ()[2]));
  PARQUET_THROW_NOT_OK(_edp_builder  .Append(hit->getEDep      ()   ));
  PARQUET_THROW_NOT_OK(_tim_builder  .Append(hit->getTime      ()   ));
  PARQUET_THROW_NOT_OK(_ptl_builder  .Append(hit->getPathLength()   ));
  PARQUET_THROW_NOT_OK(_mcidx_builder.Append(( hit->getMCParticle() ? hit->getMCParticle()->ext<CollIndex>() : -1 )));

  // common
  ObjectFile::fill(obj, evt);
}
