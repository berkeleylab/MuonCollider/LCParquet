#include "LCParquet/EventFile.hxx"

#include <arrow/io/file.h>

std::vector<std::shared_ptr<arrow::Field>> EventFile::fields()
{
  std::vector<std::shared_ptr<arrow::Field>> fields = {
    arrow::field("run",arrow::int32  ()),
    arrow::field("wgt",arrow::float64()),
    arrow::field("tim",arrow::int64  ()),
    arrow::field("sig",arrow::float32()),
    arrow::field("ene",arrow::float32()),
    arrow::field("poe",arrow::float32()),
    arrow::field("pop",arrow::float32()),
    arrow::field("pro",arrow::utf8   ())
  };

  return fields;
}

std::vector<std::shared_ptr<arrow::Array>> EventFile::arrays()
{
  std::vector<std::shared_ptr<arrow::Array>> arrays = {};
  std::shared_ptr<arrow::Array> array;

  PARQUET_THROW_NOT_OK(_run_builder.Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_wgt_builder.Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_tim_builder.Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_sig_builder.Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_ene_builder.Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_poe_builder.Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_pop_builder.Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_pro_builder.Finish(&array   ));
  arrays.push_back(array);

  return arrays;
}

void EventFile::fill(EVENT::LCEvent* evt )
{
  // hit specific
  PARQUET_THROW_NOT_OK(_run_builder.Append(evt->getRunNumber         ()));
  PARQUET_THROW_NOT_OK(_wgt_builder.Append(evt->getWeight            ()));
  PARQUET_THROW_NOT_OK(_tim_builder.Append((int64_t)evt->getTimeStamp()));
  PARQUET_THROW_NOT_OK(_sig_builder.Append(evt->getParameters().getFloatVal("CrossSection_fb")));
  PARQUET_THROW_NOT_OK(_ene_builder.Append(evt->getParameters().getFloatVal("Energy"         )));
  PARQUET_THROW_NOT_OK(_poe_builder.Append(evt->getParameters().getFloatVal("Pol_em"         )));
  PARQUET_THROW_NOT_OK(_pop_builder.Append(evt->getParameters().getFloatVal("Pol_ep"         )));
  PARQUET_THROW_NOT_OK(_pro_builder.Append(evt->getParameters().getStringVal("Process"       )));

  // common
  BaseFile::fill(evt);
}
