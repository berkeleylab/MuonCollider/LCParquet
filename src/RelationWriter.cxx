#include "LCParquet/RelationWriter.hxx"

#include <lcio.h>

#include <EVENT/LCCollection.h>
#include <EVENT/LCRelation.h>

RelationWriter::RelationWriter(std::shared_ptr<arrow::fs::FileSystem> fs, const std::string& prefix)
  : CollectionWriter()
{
  _file=std::make_shared<RelationFile>();
  _file->init(fs, prefix+".parquet");
}

RelationWriter::~RelationWriter()
{
  _file->close();
}

void RelationWriter::fill(const EVENT::LCCollection* col, const EVENT::LCEvent* evt )
{
  if( !col ) return;

  if( col->getTypeName() != lcio::LCIO::LCRELATION )
    {
      throw EVENT::Exception( "Invalid collection type: "+ col->getTypeName() ) ; 
    }

  for(int i=0, n=col->getNumberOfElements() ; i < n ; ++i)
    {
      const EVENT::LCObject *obj=col->getElementAt(i) ;
      _file->fill(obj, evt);
    }
}
