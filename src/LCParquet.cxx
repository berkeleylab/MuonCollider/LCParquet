#include "LCParquet/LCParquet.hxx"

#include <EVENT/LCCollection.h>
#include <EVENT/LCRelation.h>

#include <filesystem>

#include <arrow/filesystem/filesystem.h>

#include "LCParquet/Helpers.hxx"
#include "LCParquet/Defines.hxx"

#include "LCParquet/MCParticleWriter.hxx"
#include "LCParquet/RelationWriter.hxx"
#include "LCParquet/TrackWriter.hxx"
#include "LCParquet/TrackerHitWriter.hxx"
#include "LCParquet/SimTrackerHitWriter.hxx"

LCParquet aLCParquet ;

LCParquet::LCParquet()
  : LCParquetBase("LCParquet")
{  
  // modify processor description
  _description = "LCParquet creates Parquet files with a column wise ntuple from LCIO collections ..." ;

  // register steering parameters: name, description, class-variable, default value
  registerInputCollection( LCIO::MCPARTICLE,
			   "MCParticleCollection" , 
			   "Name of the MCParticle collection"  ,
			   _mcpColName ,
			   std::string("")
			   );

  registerInputCollection( LCIO::MCPARTICLE,
			   "MCParticleRemoveOverlayCollection" ,
			   "Name of the MCParticle collection where the overlay is removed"  ,
			   _mcpRemoveOverlayColName ,
			   std::string("")
			   );

  registerProcessorParameter( "WriteMCParticleCollectionParameters" ,
                              "Switch to write out collection parameters",
			      _mcpColWriteParameters ,
			      false
			      );
  
  registerInputCollection( LCIO::RECONSTRUCTEDPARTICLE,
			   "RecoParticleCollection" , 
			   "Name of the ReconstructedParticle collection"  ,
			   _recColName ,
			   std::string("")
			   );

  registerProcessorParameter( "WriteRecoParticleCollectionParameters" ,
                              "Switch to write out collection parameters",
			      _recColWriteParameters ,
			      false
			      );

  registerInputCollection( LCIO::RECONSTRUCTEDPARTICLE,
			   "JetCollection" , 
			   "Name of the Jet collection"  ,
			   _jetColName ,
			   std::string("")
			   );

  registerProcessorParameter( "WriteJetCollectionParameters" ,
                              "Switch to write out collection parameters",
			      _jetColWriteParameters ,
			      false
			      );
			      
  registerProcessorParameter( "JetCollectionExtraParameters" ,
                              "Switch to write out extra parameters calculated using information from Jet Finder",
			      _jetColExtraParameters ,
			      false
			      );
			      
  registerProcessorParameter( "JetCollectionTaggingParameters" ,
                              "Switch to write out jet parameters coming from LCFIPlus tagging processor",
			      _jetColTaggingParameters ,
			      false
			      );

  registerInputCollection( LCIO::RECONSTRUCTEDPARTICLE,
			   "IsoLepCollection" , 
			   "Name of the IsoLep collection"  ,
			   _isolepColName ,
			   std::string("")
			   );

  registerProcessorParameter( "WriteIsoLepCollectionParameters" ,
                              "Switch to write out collection parameters",
			      _isolepColWriteParameters ,
			      false
			      );

  registerInputCollection( LCIO::VERTEX,
			   "VertexCollection" , 
			   "Name of the Vertex collection"  ,
			   _vtxColName ,
			   std::string("")
			   );

  registerProcessorParameter( "WriteVertexCollectionParameters" ,
                              "Switch to write out collection parameters",
			      _vtxColWriteParameters ,
			      false
			      );

  registerInputCollection( LCIO::TRACK,
			   "TrackCollection" , 
			   "Name of the Track collection" ,
			   _trkColName ,
			   _trkColName
			   );

  registerProcessorParameter( "WriteTrackCollectionParameters" ,
                              "Switch to write out collection parameters",
			      _trkColWriteParameters ,
			      false
			      );

  registerInputCollection( LCIO::CLUSTER,
			   "ClusterCollection" , 
			   "Name of the Cluster collection"  ,
			   _cluColName ,
			   std::string("")
			   );

  registerProcessorParameter( "WriteClusterCollectionParameters" ,
                              "Switch to write out collection parameters",
			      _cluColWriteParameters ,
			      false
			      );

  registerInputCollection( LCIO::SIMTRACKERHIT,
			   "SimTrackerHitCollection" , 
			   "Name of the SimTrackerHit collection"  ,
			   _sthColName ,
                           _sthColName
			   );

  registerInputCollection( LCIO::TRACKERHIT,
			   "TrackerHitCollection" , 
			   "Name of the TrackerHit collection"  ,
			   _trhColName,
			   _trhColName
			   );
  
   registerProcessorParameter( "WriteSimTrackerHitCollectionParameters" ,
                              "Switch to write out collection parameters",
			      _sthColWriteParameters ,
			      false
			      );

 registerProcessorParameter( "WriteTrackerHitCollectionParameters" ,
                              "Switch to write out collection parameters",
			      _trhColWriteParameters ,
			      false
			      );

  registerInputCollection( LCIO::SIMCALORIMETERHIT,
			   "SimCalorimeterHitCollection" , 
			   "Name of the SimCalorimeterHit collection"  ,
			   _schColName ,
			   std::string("")
			   );

  registerInputCollection( LCIO::CALORIMETERHIT,
			   "CalorimeterHitCollection" , 
			   "Name of the CalorimeterHit collection"  ,
			   _cahColName ,
			   std::string("")
			   );

  registerProcessorParameter( "WriteSimCalorimeterHitCollectionParameters" ,
                              "Switch to write out collection parameters",
			      _schColWriteParameters ,
			      false
			      );
  

  registerProcessorParameter( "WriteCalorimeterHitCollectionParameters" ,
                              "Switch to write out collection parameters",
			      _cahColWriteParameters ,
			      false
			      );
  
  
  StringVec relColNames ;
  registerInputCollections( LCIO::LCRELATION,
			    "LCRelationCollections" , 
			    "Names of LCRelation collections - need parallel prefix names in RelPrefixes"  ,
			    _relColNames ,
			    relColNames
			    );
  
  StringVec relPrefixes ;
  registerProcessorParameter( "LCRelationPrefixes" , 
			      " Names of prefixes for variables from LCRelation collections - "
			      "needs to be parallel to LCRelationCollections (one prefix per collection)"  ,
			      _relPrefixes ,
			      relPrefixes
			      );

  StringVec pidBranchDefinition = {"Algorithm:TOFEstimators50ps", "tof50",
				   "TOFFirstHit", "TOFClosestHits", "TOFClosestHitsError", "TOFFlightLength", "TOFLastTrkHit", "TOFLastTrkHitFlightLength", 
				   "fh",          "ch",             "che",                 "len",             "th",            "thl"  } ;

  registerProcessorParameter( "PIDBranchDefinition" , 
			      "Definition of PID branches added to the ReconstructedParticle branches - for every algorithm:  "
			      "Algorithm:Name branchPrefix Parameter0 branch0  Parameter1 branch1  Parameter2 branch2 ..."  ,
			      _pidBranchDefinition ,
			      pidBranchDefinition
			      );

  registerInputCollection( LCIO::RECONSTRUCTEDPARTICLE,
  			   "PFOwithRelationCollection" ,
  			   "Name of the PFO collection with Relation"  ,
  			   _pfoRelName ,
  			   std::string("")
  	  	  	   );

  registerInputCollection( LCIO::LCRELATION,
  			    "LCRelationwithPFOCollections" ,
  			    "Names of LCRelation collections of PFO"  ,
  			    _relName ,
  			    std::string("")
  	  	  	  	);
}

void LCParquet::init()
{
  LCParquetBase::init();

  //
  // Create output structures
  _evtFile = std::make_shared<EventFile>();
  _evtFile->init(_fs, "event.parquet");

  if( _mcpColName.size() )
  {
    _mcpWriter = std::make_shared<MCParticleWriter>(_fs);
    //_mcpWriter->writeParameters(_mcpColWriteParameters);
  }
  
//   if( _mcpRemoveOverlayColName.size() )  {
//     _mcpremoveoverlayBranches =  new MCParticleRemoveOverlayBranches ;
//     _mcpremoveoverlayBranches->writeParameters(_mcpColWriteParameters);
//     _mcpremoveoverlayBranches->initBranches( _tree ) ;
//   }

//   if( _recColName.size() ) {
//     _recBranches =  new RecoParticleBranches ;
//     _recBranches->writeParameters(_recColWriteParameters);
//     _recBranches->initBranches( _tree ) ;

//     decodePIDBranchDefinitions() ;

//     for( auto d : _pidBranchDefs ) {

//       _pidBranchesVec.push_back( new PIDBranches ) ;
//       PIDBranches* pidb = _pidBranchesVec.back() ;
    
//       pidb->defineBranches( d.algoName, d.pNames, d.bNames ) ;
//       pidb->initBranches( _tree , d.prefix ) ;
//     }
//   }
  

//   if( _jetColName.size() ) {
//     _jetBranches = new JetBranches ;
//     _jetBranches->writeParameters(_jetColWriteParameters);
// 	_jetBranches->writeExtraParameters(_jetColExtraParameters); /* pass the value to JetBranches */
// 	_jetBranches->writeTaggingParameters(_jetColTaggingParameters); /* pass the value to JetBranches */
//     _jetBranches->initBranches( _tree );
//   }

//   if( _isolepColName.size() ) {
//     _isolepBranches = new IsoLepBranches ;
//     _isolepBranches->writeParameters(_isolepColWriteParameters);
//     _isolepBranches->initBranches( _tree ) ;
//   }

  if( _trkColName.size() )
    {
      _trkWriter =  std::make_shared<TrackWriter>(_fs) ;
    }

//   if( _cluColName.size() ) {
//     _cluBranches =  new ClusterBranches ;
//     _cluBranches->writeParameters(_cluColWriteParameters);
//     _cluBranches->initBranches( _tree ) ;
//   }
  
  if( _sthColName.size() )
    {
      _sthWriter = std::make_shared<SimTrackerHitWriter>(_fs);
    }

  if( _trhColName.size() )
    {
      _trhWriter = std::make_shared<TrackerHitWriter>(_fs);
    }
  
//   if( _schColName.size() ) {
//     _schBranches =  new SimCalorimeterHitBranches ;
//     _schBranches->writeParameters(_schColWriteParameters);
//     _schBranches->initBranches( _tree ) ;
//   }

//   if( _cahColName.size() ) {
//     _cahBranches =  new CalorimeterHitBranches ;
//     _cahBranches->writeParameters(_cahColWriteParameters);
//     _cahBranches->initBranches( _tree ) ;
//   }

//   if( _vtxColName.size() ) {
//     _vtxBranches =  new VertexBranches ;
//     _vtxBranches->writeParameters(_vtxColWriteParameters);
//     _vtxBranches->initBranches( _tree ) ;
//   }

//   if( _pfoRelName.size() && _relName.size() )  {
//       _mcRelBranches =  new MCParticleFromRelationBranches ;
//       _mcRelBranches->writeParameters(_mcpColWriteParameters);
//       _mcRelBranches->initBranches( _tree ) ;
//     }

  
  unsigned nRel =  _relColNames.size();

  if( nRel != _relPrefixes.size() )
    {
      std::stringstream ss ;
      ss  << " collection parameters LCRelationCollections and LCRelationPrefixes don't have the same length : " 
	  <<   nRel  << " != " <<  _relPrefixes.size() << std::endl
	  << " specify one prefix for every LCRelation collection !" ;

      throw Exception( ss.str() ) ;
    }

  _relWriters.resize( nRel );
  for(unsigned i=0 ; i <nRel ; ++i)
    {
      streamlog_out(DEBUG) << " initialize relation branches for collection " << _relColNames[i] << " - with prefix \""
			   << _relPrefixes[i] << "\"" << std::endl;

      _relWriters[i] = std::make_shared<RelationWriter>(_fs, _relPrefixes[i]);
    }
}

void LCParquet::processRunHeader( LCRunHeader* /*run*/)
{ } 


void LCParquet::processEvent( LCEvent * evt )
{ 
  //=====================================================
  // get the available collections from the event


  LCCollection* mcpCol =  getCollection ( evt , _mcpColName ) ;

//   LCCollection* mcpRemoveOverlayCol =  getCollection ( evt , _mcpRemoveOverlayColName ) ;

//   LCCollection* recCol =  getCollection ( evt , _recColName ) ;

//   LCCollection* jetCol =  getCollection ( evt , _jetColName ) ;
  
//   LCCollection* isolepCol =  getCollection ( evt , _isolepColName ) ;

  LCCollection* trkCol =  getCollection ( evt , _trkColName ) ;

//   LCCollection* cluCol =  getCollection ( evt , _cluColName ) ;

  LCCollection* sthCol =  getCollection ( evt , _sthColName ) ;

  LCCollection* trhCol =  getCollection ( evt , _trhColName ) ;

//   LCCollection* schCol =  getCollection ( evt , _schColName ) ;

//   LCCollection* cahCol =  getCollection ( evt , _cahColName ) ;

//   LCCollection* vtxCol =  getCollection ( evt , _vtxColName ) ;

//   LCCollection* relCol =  getCollection ( evt , _relName ) ;

//   LCCollection* pfoRelCol =  getCollection ( evt , _pfoRelName ) ;


  unsigned  nRel = _relColNames.size();
  std::vector<LCCollection*> relCols( nRel );
  for( unsigned i=0; i < nRel ; ++i)
    { relCols[i]  =  getCollection ( evt , _relColNames[i] ) ; }
  
  //=====================================================
  //     add the collection index to the objects 

  addIndexToCollection( mcpCol );

//   addIndexToCollection( mcpRemoveOverlayCol ) ;

//   addIndexToCollection( recCol ) ;

//   addIndexToCollection( jetCol ) ;
  
//   addIndexToCollection( isolepCol ) ; 

  addIndexToCollection( trkCol ) ;

//   addIndexToCollection( cluCol ) ;

//   addIndexToCollection( vtxCol ) ;

//   addIndexToCollection( relCol ) ;

//   addIndexToCollection( pfoRelCol ) ;

  addIndexToCollection( trhCol );
  addIndexToCollection( sthCol );
  for( unsigned i=0; i < nRel ; ++i)
    {
      if( relCols[i] == 0 )
	continue ;

      addIndexToCollection( relCols[i] );
    }
//   //================================================
//   //    fill the ntuple arrays 
  
  _evtFile->fill( evt );

  if( mcpCol ) _mcpWriter->fill( mcpCol , evt ) ;

//   if( mcpRemoveOverlayCol ) _mcpremoveoverlayBranches->fill( mcpRemoveOverlayCol , evt ) ;

//   if( recCol ) {
//     _recBranches->fill( recCol , evt ) ;

//     for( auto pidb : _pidBranchesVec ) pidb->fill( recCol , evt ) ;
//   }

//   if( jetCol ) _jetBranches->fill (jetCol , evt ) ;		// @ebrahimi:  removed: && jetCol->getNumberOfElements()==2
  
//   if( isolepCol ) _isolepBranches->fill (isolepCol , evt ) ;

  if( trkCol ) _trkWriter->fill( trkCol , evt ) ;

//   if( cluCol ) _cluBranches->fill( cluCol , evt ) ;

  if( sthCol ) _sthWriter->fill( sthCol , evt ) ;

  if( trhCol ) _trhWriter->fill( trhCol , evt ) ;
  
//   if( schCol ) _schBranches->fill( schCol , evt ) ;
  
//   if( cahCol ) _cahBranches->fill( cahCol , evt ) ;
  
//   if( vtxCol ) _vtxBranches->fill( vtxCol , evt ) ;

//   if( relCol && pfoRelCol ) _mcRelBranches->fill( relCol, pfoRelCol , evt ) ;

  for( unsigned i=0; i < nRel ; ++i)
    {
      if( relCols[i] == 0 )
	continue ;

      streamlog_out(DEBUG) << " filling branches for relation collection :" << _relColNames[i] 
			   << " at " << relCols[i] << std::endl ; 

      _relWriters[i]->fill(relCols[i] , evt);
    }

//   //================================================
  
  streamlog_out(DEBUG) << "   processing event: " << evt->getEventNumber() 
		       << "   in run:  "          << evt->getRunNumber()   << std::endl ;
  
}

void LCParquet::check( LCEvent * /*evt*/ )
{
  // nothing to check here - could be used to fill checkplots in reconstruction processor
}

void LCParquet::end()
{
  _evtFile->close();
}



// void LCParquet::decodePIDBranchDefinitions(){

//   std::vector<int> algoStart ;
//   const std::string algoString("Algorithm:" ) ; 

//   if( !parameterSet( "PIDBranchDefinition") )
//     return ;

//   // count the algorithms
//   for(unsigned i=0,N=_pidBranchDefinition.size();i<N;++i){
//     if( _pidBranchDefinition[i].find(algoString) != std::string::npos ){
//       algoStart.push_back(i) ;
//     }
//   }
//   int algoNum = algoStart.size();
//   _pidBranchDefs.resize( algoNum ) ;

//   algoStart.push_back( _pidBranchDefinition.size() ) ; // add one start index after end of definition string

//   for(unsigned i=0,N=_pidBranchDefs.size();i<N;++i){

//     PIDBranchDef& d = _pidBranchDefs[i] ;
//     int index = algoStart[i] ;

//     d.algoName= _pidBranchDefinition[index].substr( algoString.size(),_pidBranchDefinition[index].size() ) ;
//     ++index ;
//     d.prefix  = _pidBranchDefinition[index++] ;


//     streamlog_out( DEBUG5 ) << " found PID algorithm : " << d.algoName
// 			    << " branches will be created with prefix: " <<  d.prefix
// 			    << std::endl ;


//     int nParam =  (algoStart[i+1]-index) / 2 ;

//     streamlog_out( DEBUG5 ) << "       parameters: " ;

//     for(int j=0;j<nParam;++j){
//       d.pNames.push_back( _pidBranchDefinition[index++] ) ;
//       streamlog_out( DEBUG5 ) << d.pNames.back() << " " ;
//     }
//     streamlog_out( DEBUG5 ) << std::endl ;

//     streamlog_out( DEBUG5 ) << "       branch names: " ;

//     for(int j=0;j<nParam;++j){
//       d.bNames.push_back( _pidBranchDefinition[index++] ) ;
//       streamlog_out( DEBUG5 ) << d.bNames.back() << " " ;
//     }
//     streamlog_out( DEBUG5 ) << std::endl ;
//   } 
// }
