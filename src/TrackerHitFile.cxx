#include "LCParquet/TrackerHitFile.hxx"

#include <EVENT/TrackerHit.h>
#include <EVENT/TrackerHitPlane.h>

#include <DD4hep/Detector.h>
#include <DD4hep/DD4hepUnits.h>

#include "LCParquet/Defines.hxx"

void TrackerHitFile::init(std::shared_ptr<arrow::fs::FileSystem> fs, const std::string& path)
{
  ObjectFile::init(fs, path);

  //
  // Get detector for global to local transformations
  dd4hep::Detector& theDetector = dd4hep::Detector::getInstance();
  dd4hep::rec::SurfaceManager& surfMan = *theDetector.extension<dd4hep::rec::SurfaceManager>() ;
  dd4hep::DetElement det = theDetector.world() ;
  _map = surfMan.map( det.name() ) ;
}

std::vector<std::shared_ptr<arrow::Field>> TrackerHitFile::fields()
{
  std::vector<std::shared_ptr<arrow::Field>> fields = {
    arrow::field("ci0"  ,arrow::int32  ()),
    arrow::field("ci1"  ,arrow::int32  ()),
    arrow::field("pox"  ,arrow::float64()),
    arrow::field("poy"  ,arrow::float64()),
    arrow::field("poz"  ,arrow::float64()),
    arrow::field("edp"  ,arrow::float32()),
    arrow::field("tim"  ,arrow::float32()),
    arrow::field("covxx",arrow::float32()),
    arrow::field("covyx",arrow::float32()),
    arrow::field("covyy",arrow::float32()),
    arrow::field("covzx",arrow::float32()),
    arrow::field("covzy",arrow::float32()),
    arrow::field("covzz",arrow::float32()),
    arrow::field("pou"  ,arrow::float32()),
    arrow::field("pov"  ,arrow::float32()),
    arrow::field("covuu",arrow::float32()),
    arrow::field("covvv",arrow::float32()),
    arrow::field("covuv",arrow::float32()),
    arrow::field("typ"  ,arrow::int32  ()),
    arrow::field("qua"  ,arrow::int32  ()),
    arrow::field("ede"  ,arrow::float32())
  };

  return fields;
}

std::vector<std::shared_ptr<arrow::Array>> TrackerHitFile::arrays()
{
  std::vector<std::shared_ptr<arrow::Array>> arrays = {};
  std::shared_ptr<arrow::Array> array;

  PARQUET_THROW_NOT_OK(_ci0_builder    .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_ci1_builder    .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_pox_builder    .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_poy_builder    .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_poz_builder    .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_edp_builder    .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_tim_builder    .Finish(&array   ));
  arrays.push_back(array);

  for(int j=0;j<6;++j) {
    PARQUET_THROW_NOT_OK(_covpos_builder[j].Finish(&array   ));
    arrays.push_back(array);
  }

  PARQUET_THROW_NOT_OK(_pou_builder    .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_pov_builder    .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_covuu_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_covvv_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_covuv_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_typ_builder    .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_qua_builder    .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_ede_builder    .Finish(&array   ));
  arrays.push_back(array);

  return arrays;
}

void TrackerHitFile::fill(const EVENT::LCObject* obj, const EVENT::LCEvent* evt)
{
  const EVENT::TrackerHit* hit = dynamic_cast<const EVENT::TrackerHit*>( obj ) ;
  if(hit==0)
    { throw EVENT::Exception( "Invalid object type." ) ; }

  //
  // Global to Local transformation
  const int cellID0 = hit->getCellID0() ;

  dd4hep::rec::SurfaceMap::const_iterator sI = _map->find( cellID0 ) ;
  const dd4hep::rec::ISurface* surf = sI->second ;
  dd4hep::rec::Vector3D pos( hit->getPosition()[0], hit->getPosition()[1], hit->getPosition()[2] );
  dd4hep::rec::Vector2D lv = surf->globalToLocal( dd4hep::mm * pos  );

  float u=lv[0]/dd4hep::mm;
  float v=lv[1]/dd4hep::mm;

  // Uncertanties depend on type as there is currently
  // no easy propagation.
  float covuu=0,covvv=0, covuv=0;

  const EVENT::TrackerHitPlane *hitplane=dynamic_cast<const EVENT::TrackerHitPlane*>(hit);
  if(hitplane)
    {
      covuu=std::pow(hitplane->getdU(), 2);
      covvv=std::pow(hitplane->getdV(), 2);
    }

  //
  // Fill output file

  // hit specific
  PARQUET_THROW_NOT_OK(_ci0_builder    .Append(hit->getCellID0()));
  PARQUET_THROW_NOT_OK(_ci1_builder    .Append(hit->getCellID1()));
  PARQUET_THROW_NOT_OK(_pox_builder    .Append(hit->getPosition()[0]));
  PARQUET_THROW_NOT_OK(_poy_builder    .Append(hit->getPosition()[1]));
  PARQUET_THROW_NOT_OK(_poz_builder    .Append(hit->getPosition()[2]));
  PARQUET_THROW_NOT_OK(_edp_builder    .Append(hit->getEDep()));
  PARQUET_THROW_NOT_OK(_tim_builder    .Append(hit->getTime()));

  for(int j=0;j<6;++j) {
    PARQUET_THROW_NOT_OK(_covpos_builder[j].Append(hit->getCovMatrix()[j]));
  }

  PARQUET_THROW_NOT_OK(_pou_builder    .Append(u));
  PARQUET_THROW_NOT_OK(_pov_builder    .Append(v));
  PARQUET_THROW_NOT_OK(_covuu_builder  .Append(covuu));
  PARQUET_THROW_NOT_OK(_covvv_builder  .Append(covvv));
  PARQUET_THROW_NOT_OK(_covuv_builder  .Append(covuv));

  PARQUET_THROW_NOT_OK(_typ_builder    .Append(hit->getType()));
  PARQUET_THROW_NOT_OK(_qua_builder    .Append(hit->getQuality()));
  PARQUET_THROW_NOT_OK(_ede_builder    .Append(hit->getEDepError()));

  // Common
  ObjectFile::fill(obj, evt);
}
