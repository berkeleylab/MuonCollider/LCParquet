#include "LCParquet/LCParquetBase.hxx"

#include <EVENT/LCCollection.h>
#include <EVENT/LCRelation.h>

#include <filesystem>

#include <arrow/filesystem/filesystem.h>

#include "LCParquet/MCParticleWriter.hxx"
#include "LCParquet/RelationWriter.hxx"
#include "LCParquet/TrackWriter.hxx"
#include "LCParquet/TrackerHitWriter.hxx"
#include "LCParquet/SimTrackerHitWriter.hxx"

LCParquetBase::LCParquetBase(const std::string &procname)
  : Processor(procname)
{  
  // register steering parameters: name, description, class-variable, default value
  _outputDir=std::filesystem::current_path().string(); // Default value is current working directory
  registerProcessorParameter( "OutputDir" ,
                              "Output directory, including protocol, for saving sample.",
			      _outputDir ,
                              _outputDir
			      );
}

void LCParquetBase::init()
{  
  // usually a good idea to
  printParameters() ;

  //
  // Create output directory

  // Check if the protocol is specified
  bool foundProtocol=(_outputDir.find("://")!=std::string::npos);
  bool relPath=(_outputDir[0]!='/') && !foundProtocol;
  if(relPath)
    { _outputDir=std::filesystem::absolute(_outputDir); }

  // Create the main filesystem from output directory
  std::string internalpath;
  std::shared_ptr<arrow::fs::FileSystem> mainfs = arrow::fs::FileSystemFromUriOrPath(_outputDir, &internalpath).ValueOrDie();
  mainfs->CreateDir(internalpath); // make sure the output directory exists

  // Pass a SubTreeFileSystem to all writers such that they only have
  // to deal with relative paths.
  _fs = std::make_shared<arrow::fs::SubTreeFileSystem>(internalpath, mainfs);
}
