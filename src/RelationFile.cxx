#include "LCParquet/RelationFile.hxx"

#include <EVENT/LCRelation.h>

#include "LCParquet/Defines.hxx"

std::vector<std::shared_ptr<arrow::Field>> RelationFile::fields()
{
  std::vector<std::shared_ptr<arrow::Field>> fields = {
    arrow::field("from"  ,arrow::int64  ()),
    arrow::field("to"    ,arrow::int64  ()),
    arrow::field("weight",arrow::float32()),
  };

  return fields;
}

std::vector<std::shared_ptr<arrow::Array>> RelationFile::arrays()
{
  std::vector<std::shared_ptr<arrow::Array>> arrays = {};
  std::shared_ptr<arrow::Array> array;

  PARQUET_THROW_NOT_OK(_from_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_to_builder    .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_weight_builder.Finish(&array   ));
  arrays.push_back(array);

  return arrays;
}

void RelationFile::fill(const EVENT::LCObject* obj, const EVENT::LCEvent* evt)
{
  const EVENT::LCRelation* rel = dynamic_cast<const EVENT::LCRelation*>( obj ) ;
  if(rel==0)
    { throw EVENT::Exception( "Invalid object type." ) ; }

  // relation specific
  PARQUET_THROW_NOT_OK(_from_builder  .Append(( rel->getFrom() ? rel->getFrom()->ext<CollIndex>() : -1 )));
  PARQUET_THROW_NOT_OK(_to_builder    .Append(( rel->getTo  () ? rel->getTo  ()->ext<CollIndex>() : -1 )));
  PARQUET_THROW_NOT_OK(_weight_builder.Append(rel->getWeight()));
  
  // Common
  ObjectFile::fill(obj, evt);
}
