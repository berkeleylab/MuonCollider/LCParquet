#include "LCParquet/MCParticleFile.hxx"

#include <EVENT/MCParticle.h>

#include "LCParquet/Defines.hxx"

std::vector<std::shared_ptr<arrow::Field>> MCParticleFile::fields()
{
  std::vector<std::shared_ptr<arrow::Field>> fields = {
    arrow::field("pdg",arrow::int32  ()),
    arrow::field("gst",arrow::int32  ()),
    arrow::field("sst",arrow::int32  ()),
    arrow::field("vtx",arrow::float64()),
    arrow::field("vty",arrow::float64()),
    arrow::field("vtz",arrow::float64()),
    arrow::field("epx",arrow::float64()),
    arrow::field("epy",arrow::float64()),
    arrow::field("epz",arrow::float64()),
    arrow::field("mox",arrow::float64()),
    arrow::field("moy",arrow::float64()),
    arrow::field("moz",arrow::float64()),
    arrow::field("mas",arrow::float64()),
    arrow::field("ene",arrow::float64()),
    arrow::field("cha",arrow::float32()),
    arrow::field("tim",arrow::float32()),
    arrow::field("spx",arrow::float32()),
    arrow::field("spy",arrow::float32()),
    arrow::field("spz",arrow::float32()),
    arrow::field("cf0",arrow::int32  ()),
    arrow::field("cf1",arrow::int32  ()),
    arrow::field("pa0",arrow::int64  ()),
    arrow::field("pa1",arrow::int64  ()),
    arrow::field("da0",arrow::int64  ()),
    arrow::field("da1",arrow::int64  ()),
    arrow::field("da2",arrow::int64  ()),
    arrow::field("da3",arrow::int64  ()),
    arrow::field("da4",arrow::int64  ())
  };

  return fields;
}

std::vector<std::shared_ptr<arrow::Array>> MCParticleFile::arrays()
{
  std::vector<std::shared_ptr<arrow::Array>> arrays = {};
  std::shared_ptr<arrow::Array> array;

  PARQUET_THROW_NOT_OK(_pdg_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_gst_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_sst_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_vtx_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_vty_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_vtz_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_epx_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_epy_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_epz_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_mox_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_moy_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_moz_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_mas_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_ene_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_cha_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_tim_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_spx_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_spy_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_spz_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_cf0_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_cf1_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_pa0_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_pa1_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_da0_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_da1_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_da2_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_da3_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_da4_builder  .Finish(&array   ));
  arrays.push_back(array);

  return arrays;
}

void MCParticleFile::fill(const EVENT::LCObject* obj, const EVENT::LCEvent* evt)
{
  const EVENT::MCParticle* mcp = dynamic_cast<const EVENT::MCParticle*>( obj ) ;
  if(mcp==0)
    { throw EVENT::Exception( "Invalid object type." ) ; }

  // MC specific
  PARQUET_THROW_NOT_OK(_pdg_builder  .Append(mcp->getPDG()));
  PARQUET_THROW_NOT_OK(_gst_builder  .Append(mcp->getGeneratorStatus()));
  PARQUET_THROW_NOT_OK(_sst_builder  .Append(mcp->getSimulatorStatus()));
  PARQUET_THROW_NOT_OK(_vtx_builder  .Append(mcp->getVertex()[0]));
  PARQUET_THROW_NOT_OK(_vty_builder  .Append(mcp->getVertex()[1]));
  PARQUET_THROW_NOT_OK(_vtz_builder  .Append(mcp->getVertex()[2]));
  PARQUET_THROW_NOT_OK(_epx_builder  .Append(mcp->getEndpoint()[0]));
  PARQUET_THROW_NOT_OK(_epy_builder  .Append(mcp->getEndpoint()[1]));
  PARQUET_THROW_NOT_OK(_epz_builder  .Append(mcp->getEndpoint()[2]));
  PARQUET_THROW_NOT_OK(_mox_builder  .Append(mcp->getMomentum()[0]));
  PARQUET_THROW_NOT_OK(_moy_builder  .Append(mcp->getMomentum()[1]));
  PARQUET_THROW_NOT_OK(_moz_builder  .Append(mcp->getMomentum()[2]));
  PARQUET_THROW_NOT_OK(_mas_builder  .Append(mcp->getMass()));
  PARQUET_THROW_NOT_OK(_ene_builder  .Append(mcp->getEnergy()));
  PARQUET_THROW_NOT_OK(_cha_builder  .Append(mcp->getCharge()));
  PARQUET_THROW_NOT_OK(_tim_builder  .Append(mcp->getTime()));
  PARQUET_THROW_NOT_OK(_spx_builder  .Append(mcp->getSpin()[0]));
  PARQUET_THROW_NOT_OK(_spy_builder  .Append(mcp->getSpin()[1]));
  PARQUET_THROW_NOT_OK(_spz_builder  .Append(mcp->getSpin()[2]));
  PARQUET_THROW_NOT_OK(_cf0_builder  .Append(mcp->getColorFlow()[0]));
  PARQUET_THROW_NOT_OK(_cf1_builder  .Append(mcp->getColorFlow()[1]));

  // Parents
  const EVENT::MCParticleVec& p = mcp->getParents() ;

  // set the parents index - if parents exist; -1 otherwise
  PARQUET_THROW_NOT_OK(_pa0_builder  .Append( ( p.size() > 0 ?  p[0]->ext<CollIndex>() :  -1 ) ));
  PARQUET_THROW_NOT_OK(_pa1_builder  .Append( ( p.size() > 1 ?  p[1]->ext<CollIndex>() :  -1 ) ));
  // can we have more than two parents ????

  // set the daughter index - if daughter exsts; -1 otherwise
  const EVENT::MCParticleVec& d = mcp->getDaughters() ;

  PARQUET_THROW_NOT_OK(_da0_builder  .Append(( d.size() > 0 ?  d[0]->ext<CollIndex>() :  -1 ) ));
  PARQUET_THROW_NOT_OK(_da1_builder  .Append(( d.size() > 1 ?  d[1]->ext<CollIndex>() :  -1 ) ));
  PARQUET_THROW_NOT_OK(_da2_builder  .Append(( d.size() > 2 ?  d[2]->ext<CollIndex>() :  -1 ) ));
  PARQUET_THROW_NOT_OK(_da3_builder  .Append(( d.size() > 3 ?  d[3]->ext<CollIndex>() :  -1 ) ));
  PARQUET_THROW_NOT_OK(_da4_builder  .Append(( d.size() > 4 ?  d[4]->ext<CollIndex>() :  -1 ) ));

  // Common
  ObjectFile::fill(obj, evt);
}
