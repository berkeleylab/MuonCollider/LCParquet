#include "LCParquet/SimTrackerHitWriter.hxx"

#include <lcio.h>

#include <EVENT/LCCollection.h>
#include <EVENT/SimTrackerHit.h>

SimTrackerHitWriter::SimTrackerHitWriter(std::shared_ptr<arrow::fs::FileSystem> fs)
  : CollectionWriter()
{
  _file=std::make_shared<SimTrackerHitFile>();
  _file->init(fs, "simtrackerhit.parquet");
}

SimTrackerHitWriter::~SimTrackerHitWriter()
{
  _file->close();
}

void SimTrackerHitWriter::fill(const EVENT::LCCollection* col, const EVENT::LCEvent* evt )
{
  if( !col ) return;

  if( col->getTypeName() != lcio::LCIO::SIMTRACKERHIT )
    {
      throw EVENT::Exception( "Invalid collection type: "+ col->getTypeName() ) ; 
    }

  for(int i=0, n=col->getNumberOfElements() ; i < n ; ++i)
    {
      EVENT::LCObject *obj=col->getElementAt(i) ;
      _file->fill(obj, evt);
    }
}
