#include "LCParquet/MCParticleWriter.hxx"

#include <lcio.h>

#include <EVENT/LCCollection.h>
#include <EVENT/MCParticle.h>

MCParticleWriter::MCParticleWriter(std::shared_ptr<arrow::fs::FileSystem> fs)
  : CollectionWriter()
{
  _file=std::make_shared<MCParticleFile>();
  _file->init(fs, "mc.parquet");
}

MCParticleWriter::~MCParticleWriter()
{
  _file->close();
}

void MCParticleWriter::fill(const EVENT::LCCollection* col, const EVENT::LCEvent* evt )
{
  if( !col ) return;

  if( col->getTypeName() != lcio::LCIO::MCPARTICLE )
    {
      throw EVENT::Exception( "Invalid collection type: "+ col->getTypeName() ) ; 
    }

  for(int i=0, n=col->getNumberOfElements() ; i < n ; ++i)
    {
      EVENT::LCObject *obj=col->getElementAt(i) ;
      _file->fill(obj, evt);
    }
}
