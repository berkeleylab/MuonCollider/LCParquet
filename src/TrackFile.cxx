#include "LCParquet/TrackFile.hxx"

#include <EVENT/Track.h>

#include "LCParquet/Defines.hxx"

std::vector<std::shared_ptr<arrow::Field>> TrackFile::fields()
{
  std::vector<std::shared_ptr<arrow::Field>> fields = {
    arrow::field("typ",arrow::int32  ()),
    arrow::field("ch2",arrow::float32()),
    arrow::field("ndf",arrow::int32  ()),
    arrow::field("edx",arrow::float32()),
    arrow::field("ede",arrow::float32()),
    arrow::field("rih",arrow::float32()),
    arrow::field("sip",arrow::int64  ()),
    arrow::field("sfh",arrow::int64  ()),
    arrow::field("slh",arrow::int64  ()),
    arrow::field("sca",arrow::int64  ())
  };

  return fields;
}

std::vector<std::shared_ptr<arrow::Array>> TrackFile::arrays()
{
  std::vector<std::shared_ptr<arrow::Array>> arrays = {};
  std::shared_ptr<arrow::Array> array;

  PARQUET_THROW_NOT_OK(_typ_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_ch2_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_ndf_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_edx_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_ede_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_rih_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_sip_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_sfh_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_slh_builder  .Finish(&array   ));
  arrays.push_back(array);

  PARQUET_THROW_NOT_OK(_sca_builder  .Finish(&array   ));
  arrays.push_back(array);

  return arrays;
}

void TrackFile::fill(const EVENT::LCObject* obj, const EVENT::LCEvent* evt)
{
  const EVENT::Track* trk = dynamic_cast<const EVENT::Track*>( obj ) ;
  if(trk==0)
    { throw EVENT::Exception( "Invalid object type." ) ; }


  // Track specific
  PARQUET_THROW_NOT_OK(_typ_builder  .Append(trk->getType()));
  PARQUET_THROW_NOT_OK(_ch2_builder  .Append(trk->getChi2()));
  PARQUET_THROW_NOT_OK(_ndf_builder  .Append(trk->getNdf()));
  PARQUET_THROW_NOT_OK(_edx_builder  .Append(trk->getdEdx()));
  PARQUET_THROW_NOT_OK(_ede_builder  .Append(trk->getdEdxError()));
  PARQUET_THROW_NOT_OK(_rih_builder  .Append(trk->getRadiusOfInnermostHit()));

  const EVENT::TrackState* ts ;

  ts = trk->getTrackState( EVENT::TrackState::AtIP          );
  PARQUET_THROW_NOT_OK(_sip_builder  .Append(( ts ?  ts->ext<CollIndex>() : -1 ) ));

  ts = trk->getTrackState( EVENT::TrackState::AtFirstHit    );
  PARQUET_THROW_NOT_OK(_sfh_builder  .Append(( ts ?  ts->ext<CollIndex>() : -1 ) ));

  ts = trk->getTrackState( EVENT::TrackState::AtLastHit     );
  PARQUET_THROW_NOT_OK(_slh_builder  .Append(( ts ?  ts->ext<CollIndex>() : -1 ) ));

  ts = trk->getTrackState( EVENT::TrackState::AtCalorimeter );
  PARQUET_THROW_NOT_OK(_sca_builder  .Append(( ts ?  ts->ext<CollIndex>() : -1 ) ));

  // Common
  ObjectFile::fill(obj, evt);
}
