#pragma once

#include "LCParquet/ObjectFile.hxx"

//! Stores data from Relation collection
class RelationFile : public ObjectFile
{
public:
  RelationFile() =default;
  virtual ~RelationFile() =default;

  virtual void fill(const EVENT::LCObject* obj, const EVENT::LCEvent* evt);

protected:
  virtual std::vector<std::shared_ptr<arrow::Field>> fields();
  virtual std::vector<std::shared_ptr<arrow::Array>> arrays();

private:
  //! Builders
  arrow::Int64Builder _from_builder;
  arrow::Int64Builder _to_builder;
  arrow::FloatBuilder _weight_builder;
};
