#pragma once

#include "ObjectFile.hxx"

//! Stores data from SimTrackerHit collection
class SimTrackerHitFile : public ObjectFile
{
public:
  SimTrackerHitFile() =default;
  virtual ~SimTrackerHitFile() =default;

  virtual void fill(const EVENT::LCObject* obj, const EVENT::LCEvent* evt);

protected:
  virtual std::vector<std::shared_ptr<arrow::Field>> fields();
  virtual std::vector<std::shared_ptr<arrow::Array>> arrays();

private:
  //! Builders
  arrow::Int32Builder  _ci0_builder;
  arrow::Int32Builder  _ci1_builder;
  arrow::DoubleBuilder _pox_builder;
  arrow::DoubleBuilder _poy_builder;
  arrow::DoubleBuilder _poz_builder;
  arrow::FloatBuilder  _edp_builder;
  arrow::FloatBuilder  _tim_builder;
  arrow::FloatBuilder  _ptl_builder;
  arrow::Int64Builder  _mcidx_builder;
};
