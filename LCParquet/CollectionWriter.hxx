#pragma once

#include <string>

#include <EVENT/LCEvent.h>

#include <parquet/stream_writer.h>

//! Interface for writer classes
/**
 * Writer classes save an `LCCollection` to a series
 * of `CollectionFile` objects.
 */
class CollectionWriter
{
public:
  CollectionWriter() =default;
  virtual ~CollectionWriter() =default;

  virtual void fill(const EVENT::LCCollection* col, const EVENT::LCEvent* evt ) =0;

  //virtual void writeParameters(bool writeparameters){ _writeparameters=writeparameters; };

};
