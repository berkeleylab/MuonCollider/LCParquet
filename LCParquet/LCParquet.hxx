#pragma once

#include "LCParquet/LCParquetBase.hxx"

#include "LCParquet/EventFile.hxx"
#include "LCParquet/CollectionWriter.hxx"

//! Creates a simple column wise ntuple in a Parquet from LCIO collections.
/** 
 * ** Input - Prerequisites **
 * Needs collections of MCParticles, ReconstructedParticles, Tracks, ....
 * There can only be one collection of every type (users should copy all collections
 * of a given type into one (subset) collection).
 *
 * ** Output **
 *  A Parquet file with LCIO data in column wise format
 */
class LCParquet : public LCParquetBase
{
  /*
  struct PIDBranchDef{
    std::string algoName{} ;
    std::string prefix{} ;
    std::vector<std::string> pNames{} ;
    std::vector<std::string> bNames{} ;
  } ;
  */

public:
  virtual Processor*  newProcessor() { return new LCParquet ; }

  LCParquet(const LCParquet &) = delete ;
  LCParquet& operator =(const LCParquet &) = delete ;
  LCParquet() ;

  /** Called at the begin of the job before anything is read.
   * Use to initialize the processor, e.g. book histograms.
   */
  virtual void init() ;

  /** Called for every run.
   */
  virtual void processRunHeader( LCRunHeader* run ) ;

  /** Called for every event - the working horse.
   */
  virtual void processEvent( LCEvent * evt ) ; 

  virtual void check( LCEvent * evt ) ; 


  /** Called after data processing for clean up.
   */
  virtual void end() ;  

 protected:

  // void decodePIDBranchDefinitions() ;

  /** Input collection name.
   */
  std::string _mcpColName {};
  std::string _mcpRemoveOverlayColName {};
  std::string _recColName {};
  std::string _jetColName {};
  std::string _isolepColName {};  
  std::string _trkColName {};
  std::string _cluColName {};
  std::string _sthColName {};
  std::string _trhColName {};
  std::string _schColName {};
  std::string _cahColName {};
  std::string _vtxColName {};
  std::string _pfoRelName {};
  std::string _relName {};

  bool _mcpColWriteParameters {};
  bool _recColWriteParameters {};
  bool _jetColWriteParameters {};
  bool _isolepColWriteParameters {}; 
  bool _trkColWriteParameters {};
  bool _cluColWriteParameters {};
  bool _sthColWriteParameters {};
  bool _trhColWriteParameters {};
  bool _schColWriteParameters {};
  bool _cahColWriteParameters {};
  bool _vtxColWriteParameters {};


  bool _jetColExtraParameters {};                 /* Enables writing extra jet parameters */
  bool _jetColTaggingParameters {};               /* Enables writing jet tagging parameters */

  StringVec _relColNames {};
  StringVec _relPrefixes {};

  StringVec _pidBranchDefinition {};
  
  std::shared_ptr<EventFile> _evtFile;
  std::shared_ptr<CollectionWriter> _mcpWriter;
//   CollectionBranches* _mcpremoveoverlayBranches {};
//   CollectionBranches* _recBranches {};
// //  CollectionBranches* _jetBranches {};
//   JetBranches* _jetBranches {};
//   CollectionBranches* _isolepBranches {};
  std::shared_ptr<CollectionWriter> _trkWriter;
//   CollectionBranches* _cluBranches {};
//   CollectionBranches* _sthBranches {};
  std::shared_ptr<CollectionWriter> _sthWriter;
  std::shared_ptr<CollectionWriter> _trhWriter;
//   CollectionBranches* _schBranches {};
//   CollectionBranches* _cahBranches {};
//   CollectionBranches* _vtxBranches {};
//   MCParticleFromRelationBranches* _mcRelBranches {};
  
//   std::vector<PIDBranches*> _pidBranchesVec {};
  std::vector<std::shared_ptr<CollectionWriter>> _relWriters;

//   std::vector<PIDBranchDef> _pidBranchDefs {} ;
};
