#pragma once

#include <EVENT/LCCollection.h>

//! Safely retrieve a collection
/**
 * \param evt Event store
 * \param name Name of collection to retrieve
 *
 * \return Collection if found, nullptr otherwise.
 */
inline lcio::LCCollection* getCollection(lcio::LCEvent* evt, const std::string& name )
{
  if( name.size() == 0 )
    return nullptr;

  try
    { return evt->getCollection( name ); }
  catch( lcio::DataNotAvailableException& e )
    {
      streamlog_out( DEBUG2 ) << "getCollection :  DataNotAvailableException : " << name <<  std::endl ;
      return nullptr;
    }
}

//! Add an index extension to all elements in a collection
/**
 * \param col Collection to add indexes to.
 */
inline void addIndexToCollection( lcio::LCCollection* col )
{
  if( col == nullptr )
    { return; }

  for(int i=0, n  = col->getNumberOfElements() ; i < n; ++i )
    {
      col->getElementAt( i )->ext<CollIndex>() = i ;
    }
}

