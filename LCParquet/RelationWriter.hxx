#pragma once

#include "CollectionWriter.hxx"
#include "RelationFile.hxx"

//! Stores data from Relation collection
class RelationWriter : public CollectionWriter
{
public:
  RelationWriter(std::shared_ptr<arrow::fs::FileSystem> fs, const std::string& prefix);
  virtual ~RelationWriter();

  virtual void fill(const EVENT::LCCollection* col, const EVENT::LCEvent* evt ) ;

protected:
  std::shared_ptr<RelationFile> _file;
};

