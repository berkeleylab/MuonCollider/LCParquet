#pragma once

#include <arrow/filesystem/filesystem.h>

#include "CollectionWriter.hxx"
#include "TrackerHitFile.hxx"

//! Stores data from TrackerHit collection
class TrackerHitWriter : public CollectionWriter
{
public:
  TrackerHitWriter(std::shared_ptr<arrow::fs::FileSystem> fs);
  virtual ~TrackerHitWriter();

  virtual void fill(const EVENT::LCCollection* col, const EVENT::LCEvent* evt ) ;

protected:
  std::shared_ptr<TrackerHitFile> _file;
};

