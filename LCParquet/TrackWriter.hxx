#pragma once

#include <arrow/filesystem/filesystem.h>

#include "CollectionWriter.hxx"
#include "TrackFile.hxx"
#include "TrackStateFile.hxx"

//! Stores data from Track collection
class TrackWriter : public CollectionWriter
{
public:
  TrackWriter(std::shared_ptr<arrow::fs::FileSystem> fs);
  virtual ~TrackWriter();

  virtual void fill(const EVENT::LCCollection* col, const EVENT::LCEvent* evt ) ;

protected:
  std::shared_ptr<TrackFile> _tr;
  std::shared_ptr<TrackStateFile> _ts;
};

