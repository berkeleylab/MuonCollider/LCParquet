#pragma once

#include "ObjectFile.hxx"

//! Stores data from Track collection
class TrackFile : public ObjectFile
{
public:
  TrackFile() =default;
  virtual ~TrackFile() =default;

  virtual void fill(const EVENT::LCObject* obj, const EVENT::LCEvent* evt);

protected:
  virtual std::vector<std::shared_ptr<arrow::Field>> fields();
  virtual std::vector<std::shared_ptr<arrow::Array>> arrays();

private:
  //! Builders
  arrow::Int32Builder  _typ_builder;
  arrow::FloatBuilder  _ch2_builder;
  arrow::Int32Builder  _ndf_builder;
  arrow::FloatBuilder  _edx_builder;
  arrow::FloatBuilder  _ede_builder;
  arrow::FloatBuilder  _rih_builder;
  arrow::Int64Builder  _sip_builder;
  arrow::Int64Builder  _sfh_builder;
  arrow::Int64Builder  _slh_builder;
  arrow::Int64Builder  _sca_builder;
};
