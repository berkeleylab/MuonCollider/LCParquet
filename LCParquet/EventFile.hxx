#pragma once

#include "BaseFile.hxx"

#include <EVENT/LCEvent.h>

//! Holds columns with data from LCEvent
class EventFile : public BaseFile
{ 
public: 
  EventFile() =default;
  virtual ~EventFile() =default;

  virtual void fill(EVENT::LCEvent* evt );

protected:
  virtual std::vector<std::shared_ptr<arrow::Field>> fields();
  virtual std::vector<std::shared_ptr<arrow::Array>> arrays();

private:
  //! Builders
  arrow::Int32Builder  _run_builder;
  arrow::DoubleBuilder _wgt_builder;
  arrow::Int64Builder  _tim_builder;
  arrow::FloatBuilder  _sig_builder;
  arrow::FloatBuilder  _ene_builder;
  arrow::FloatBuilder  _poe_builder;
  arrow::FloatBuilder  _pop_builder;
  arrow::StringBuilder _pro_builder;
};
