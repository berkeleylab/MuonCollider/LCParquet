#pragma once

#include <marlin/Processor.h>

#include "EventFile.hxx"
#include "CollectionWriter.hxx"

//! A base class for Processors that output Parquet file formats
/**
 * @author Karol Krizka
 * @version $Id$
 */
class LCParquetBase : public marlin::Processor
{
public:
  LCParquetBase(const LCParquetBase &) = delete ;
  LCParquetBase& operator =(const LCParquetBase &) = delete ;
  LCParquetBase(const std::string &procname) ;

  //! Creates the `_fs` filesystem
  /**
   * All subclasses should call this before creating
   * any writers.
   */
  virtual void init() ;

 protected:

  //! Filesystem for saving output files
  std::shared_ptr<arrow::fs::FileSystem> _fs;

  /** Output
   */
  std::string _outputDir {"data/"};
};
