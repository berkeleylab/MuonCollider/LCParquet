#pragma once

#include "ObjectFile.hxx"

//! Stores data from TrackState collection
class TrackStateFile : public ObjectFile
{
public:
  TrackStateFile() =default;
  virtual ~TrackStateFile() =default;

  virtual void fill(const EVENT::LCObject* obj, const EVENT::LCEvent* evt);

protected:
  virtual std::vector<std::shared_ptr<arrow::Field>> fields();
  virtual std::vector<std::shared_ptr<arrow::Array>> arrays();

private:
  //! Builders
  arrow::Int32Builder _loc_builder;
  arrow::FloatBuilder _dze_builder;
  arrow::FloatBuilder _phi_builder;
  arrow::FloatBuilder _ome_builder;
  arrow::FloatBuilder _zze_builder;
  arrow::FloatBuilder _tnl_builder;
  arrow::FloatBuilder _rpx_builder;
  arrow::FloatBuilder _rpy_builder;
  arrow::FloatBuilder _rpz_builder;
  arrow::FloatBuilder _cov_builder[15];
};
