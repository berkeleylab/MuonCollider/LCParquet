#pragma once

#include <arrow/filesystem/filesystem.h>

#include "CollectionWriter.hxx"
#include "SimTrackerHitFile.hxx"

//! Stores data from SimTrackerHit collection
class SimTrackerHitWriter : public CollectionWriter
{
public:
  SimTrackerHitWriter(std::shared_ptr<arrow::fs::FileSystem> fs);
  virtual ~SimTrackerHitWriter();

  virtual void fill(const EVENT::LCCollection* col, const EVENT::LCEvent* evt ) ;

protected:
  std::shared_ptr<SimTrackerHitFile> _file;
};
