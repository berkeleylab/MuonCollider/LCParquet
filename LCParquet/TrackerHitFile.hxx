#pragma once

#include <DDRec/SurfaceManager.h>

#include "ObjectFile.hxx"

//! Stores data from TrackerHit collection
class TrackerHitFile : public ObjectFile
{
public:
  TrackerHitFile() =default;
  virtual ~TrackerHitFile() =default;

  virtual void init(std::shared_ptr<arrow::fs::FileSystem> fs, const std::string& path);
  virtual void fill(const EVENT::LCObject* obj, const EVENT::LCEvent* evt);

protected:
  virtual std::vector<std::shared_ptr<arrow::Field>> fields();
  virtual std::vector<std::shared_ptr<arrow::Array>> arrays();

private:
  const dd4hep::rec::SurfaceMap* _map = nullptr;

  //! Builders
  arrow::Int32Builder  _ci0_builder  ;
  arrow::Int32Builder  _ci1_builder  ;
  arrow::DoubleBuilder _pox_builder  ;
  arrow::DoubleBuilder _poy_builder  ;
  arrow::DoubleBuilder _poz_builder  ;
  arrow::FloatBuilder  _edp_builder  ;
  arrow::FloatBuilder  _tim_builder  ;
  arrow::FloatBuilder  _covpos_builder[6]; // xx, yx, yy, zx, zy, zz
  arrow::FloatBuilder  _pou_builder  ;
  arrow::FloatBuilder  _pov_builder  ;
  arrow::FloatBuilder  _covuu_builder;
  arrow::FloatBuilder  _covvv_builder;
  arrow::FloatBuilder  _covuv_builder;
  arrow::Int32Builder  _typ_builder  ;
  arrow::Int32Builder  _qua_builder  ;
  arrow::FloatBuilder  _ede_builder  ;
};



