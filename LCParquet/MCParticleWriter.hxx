#pragma once

#include <arrow/filesystem/filesystem.h>

#include "CollectionWriter.hxx"
#include "MCParticleFile.hxx"

//! Stores data from MCParticle collection
class MCParticleWriter : public CollectionWriter
{
public:
  MCParticleWriter(std::shared_ptr<arrow::fs::FileSystem> fs);
  virtual ~MCParticleWriter();

  virtual void fill(const EVENT::LCCollection* col, const EVENT::LCEvent* evt ) ;

protected:
  std::shared_ptr<MCParticleFile> _file;
};
