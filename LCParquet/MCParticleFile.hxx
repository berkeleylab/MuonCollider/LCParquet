#pragma once

#include "ObjectFile.hxx"

//! Stores data from MCParticle collection
class MCParticleFile : public ObjectFile
{
public:
  MCParticleFile() =default;
  virtual ~MCParticleFile() =default;

  virtual void fill(const EVENT::LCObject* obj, const EVENT::LCEvent* evt);

protected:
  virtual std::vector<std::shared_ptr<arrow::Field>> fields();
  virtual std::vector<std::shared_ptr<arrow::Array>> arrays();

private:
  //! Builders
  arrow::Int32Builder  _pdg_builder;
  arrow::Int32Builder  _gst_builder;
  arrow::Int32Builder  _sst_builder;
  arrow::DoubleBuilder _vtx_builder;
  arrow::DoubleBuilder _vty_builder;
  arrow::DoubleBuilder _vtz_builder;
  arrow::DoubleBuilder _epx_builder;
  arrow::DoubleBuilder _epy_builder;
  arrow::DoubleBuilder _epz_builder;
  arrow::DoubleBuilder _mox_builder;
  arrow::DoubleBuilder _moy_builder;
  arrow::DoubleBuilder _moz_builder;
  arrow::DoubleBuilder _mas_builder;
  arrow::DoubleBuilder _ene_builder;
  arrow::FloatBuilder  _cha_builder;
  arrow::FloatBuilder  _tim_builder;
  arrow::FloatBuilder  _spx_builder;
  arrow::FloatBuilder  _spy_builder;
  arrow::FloatBuilder  _spz_builder;
  arrow::Int32Builder  _cf0_builder;
  arrow::Int32Builder  _cf1_builder;
  arrow::Int64Builder  _pa0_builder;
  arrow::Int64Builder  _pa1_builder;
  arrow::Int64Builder  _da0_builder;
  arrow::Int64Builder  _da1_builder;
  arrow::Int64Builder  _da2_builder;
  arrow::Int64Builder  _da3_builder;
  arrow::Int64Builder  _da4_builder;
};
